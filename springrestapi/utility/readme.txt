#Rest API sample

###Call-1###
Request
    POST | http://localhost:8080/springrestapi/rest/payment/pay?key=SHARED_KEY
Parameter
    body > raw > JSON (application/json)
        {
            "userId":"1",
            "itemId":"item1"
        }
output
    {
        "status": "success",
        "code": 100
    }

###Call-2###
Request
    GET | http://localhost:8080/springrestapi/rest/payment/transaction?key=SHARED_KEY
output
    [
        {
            "username": "admin",
            "item": "Snack",
            "discount": 30,
            "amount": 50
        }
    ]