package com.snippet.config;

import com.snippet.bo.request.PaymentRequest;
import com.snippet.bo.response.impl.PaymentResponseImpl;
import com.snippet.repository.impl.PayImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Spring configuration includes datasource and jdbc configuration"
 * */
@Configuration
@PropertySource(value = {"classpath:datasource.properties"})
public class SpringConfiguration {

    @Autowired
    private Environment env;

    /**
     * @return instance of {@link PaymentRequest}
     */
    @Bean(name = "payRequest")
    PaymentRequest getPaymentRequest() {
        return new PaymentRequest();
    }

    /**
     * @return instance of {@link PaymentResponseImpl}
     */
    @Bean(name = "payResponse")
    PaymentResponseImpl getPaymentResponse(){
        return new PaymentResponseImpl();
    }

    /**
     * @return instance of {@link PayImpl}
     */
    @Bean(name = "pay")
    PayImpl getPaymentImpl() {
        return new PayImpl();
    }

    /**
     * @return configured data source
     */
    @Bean
    DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
        return dataSource;
    }

    /**
     * @param dataSource configured
     * @return jdbcTemplate ready with implemented data source
     */
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }
}
