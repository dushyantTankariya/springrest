package com.snippet.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Application configuration goes here ({@link EnableWebMvc} & {@link ComponentScan})"
 */
@EnableWebMvc
@ComponentScan(basePackages = "com.snippet")
public class ApplicationConfiguration {

}
