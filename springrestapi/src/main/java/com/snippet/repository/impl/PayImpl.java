package com.snippet.repository.impl;

import com.snippet.bo.request.PaymentRequest;
import com.snippet.bo.response.impl.PaymentResponseImpl;
import com.snippet.repository.Pay;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Database Transaction for Payment"
 * */
@Repository
public class PayImpl implements Pay {

    private Logger logger = Logger.getLogger(PayImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * @see "Default Constructor"
     */
    public PayImpl() {
    }

    /**
     * @param paymentRequest {@link PaymentRequest}
     * @see "To story payment information"
     */
    @Override
    public void insert(PaymentRequest paymentRequest) {
        logger.trace("Payment Request called");
        String sql = "INSERT INTO payment (user_id, item_id, discount, amount) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, paymentRequest.getPaymentRequestToObjectArrayFactory());
        logger.trace("payment Request responded");
    }

    /**
     * @return {@link List < PaymentResponseImpl >}
     * @see "To list all the payment transaction"
     */
    @Override
    public List<PaymentResponseImpl> findAll() {
        List<PaymentResponseImpl> payments;
        String sql = "select username, item, discount, amount" +
                " from payment p" +
                " left outer join user u on (p.user_id = u.user_id)" +
                " left outer join item i on (p.item_id = i.item_id)";
        payments = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(PaymentResponseImpl.class));
        return payments;
    }
}
