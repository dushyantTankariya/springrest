package com.snippet.repository;

import com.snippet.bo.request.PaymentRequest;
import com.snippet.bo.response.impl.PaymentResponseImpl;

import java.util.List;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * */
public interface Pay {
    /**
     * @param paymentRequest {@link PaymentRequest}
     * @see "To store payment information"
     * */
    void insert(PaymentRequest paymentRequest);

    /**
     * @return {@link List<PaymentResponseImpl>}
     * @see "To list all the payment transaction"
     * */
    List<PaymentResponseImpl> findAll();
}
