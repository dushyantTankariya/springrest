package com.snippet.springrestapi;

import com.snippet.bo.request.PaymentRequest;
import com.snippet.bo.response.Response;
import com.snippet.bo.response.impl.BaseResponseImpl;
import com.snippet.config.SpringConfiguration;
import com.snippet.helper.PaymentHelper;
import com.snippet.repository.Pay;
import com.snippet.repository.impl.PayImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.snippet.enums.BaseResponse.SHARED_KEY;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest controller for payment"
 * */
@RestController
@RequestMapping("/payment")
public class PaymentController {

    private Logger logger;

    @Autowired
    private Pay pay;

    {
        logger = Logger.getLogger(PaymentController.class);
    }

    /**
     * @see "To store payment details"
     * @param key a shared secret
     * @param request entity
     * @return {@link Response}
     */
    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public Response pay(@RequestParam(value = "key") String key, @RequestBody PaymentRequest request) {
        AbstractApplicationContext applicationContext;
        applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        logger.trace("pay controller called.");
        Response response;
        PaymentHelper paymentHelper = new PaymentHelper();

        if (SHARED_KEY.value.equalsIgnoreCase(key)) {
            logger.debug("application get pay Bean");
            pay = applicationContext.getBean("pay", PayImpl.class);
            pay.insert(request);
            response = paymentHelper.getSuccessBaseResponse();
        } else {
            response = paymentHelper.getFailBaseResponse();
        }
        logger.trace("pay controller responded.");
        applicationContext.close();
        return response;
    }


    /**
     * @see "List of all transaction"
     * @param key a shared secret
     * @return {@link List<Response>}
     */
    @RequestMapping(value = "/transaction", method = RequestMethod.GET)
    public List<Response> transaction(@RequestParam(value = "key") String key) {
        AbstractApplicationContext applicationContext;
        applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        logger.trace("transaction controller called.");
        List<Response> response;
        PaymentHelper paymentHelper = new PaymentHelper();

        if (SHARED_KEY.value.equalsIgnoreCase(key)) {
            logger.debug("application get pay Bean");
            pay = applicationContext.getBean("pay", PayImpl.class);
            response = new ArrayList<>(pay.findAll());
        } else {
            response = new ArrayList<>();
            response.add(paymentHelper.getFailBaseResponse());
        }
        logger.trace("pay controller responded.");
        applicationContext.close();
        return response;
    }
}
