package com.snippet.helper;

import com.snippet.bo.response.Response;
import com.snippet.bo.response.impl.BaseResponseImpl;

import static com.snippet.enums.BaseResponse.*;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Payment helper class contains helper methods during payments"
 * */
public class PaymentHelper {

    /**
     * @see "Default constructor"
     */
    public PaymentHelper() {
    }

    /**
     * @return baseResponse {@link Response} with http success response
     */
    public Response getSuccessBaseResponse() {
        BaseResponseImpl baseResponse = new BaseResponseImpl();
        baseResponse.setStatus(SUCCESS_STATUS.value);
        baseResponse.setCode(CODE_SUCCESS.intValue);
        return baseResponse;
    }

    /**
     * @return baseResponse {@link Response} with http auth failure response
     */
    public Response getFailBaseResponse() {
        BaseResponseImpl baseResponse = new BaseResponseImpl();
        baseResponse.setStatus(ERROR_STATUS.value);
        baseResponse.setCode(AUTH_FAILURE.intValue);
        return baseResponse;
    }

}
