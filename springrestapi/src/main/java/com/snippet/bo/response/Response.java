package com.snippet.bo.response;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Interface to implement in {@link com.snippet.bo.response.impl.BaseResponseImpl}, and {@link com.snippet.bo.response.impl.PaymentResponseImpl}"
 * */
public interface Response {
}
