package com.snippet.bo.response.impl;

import com.snippet.bo.response.Response;
import org.springframework.stereotype.Component;

/**
 * @author Dusyant Tankariya
 * @since 1.0.0.0
 * */
@Component
public class BaseResponseImpl implements Response {
    private String status;
    private Integer code;

    /**
     * @see "default constructor"
     */
    public BaseResponseImpl() {
    }

    /**
     * @see "parameterized constructor"
     * @param status of http response
     * @param code of http response
     */
    public BaseResponseImpl(String status, Integer code) {
        this.status = status;
        this.code = code;
    }

    /**
     * @return status of http response
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status of http response
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return code of http response
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code of http response
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return string representation of entity
     */
    @Override
    public String toString() {
        return "BaseResponse{" +
                "status='" + status + '\'' +
                ", code=" + code +
                '}';
    }
}
