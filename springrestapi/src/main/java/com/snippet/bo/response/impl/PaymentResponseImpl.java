package com.snippet.bo.response.impl;

import com.snippet.bo.response.Response;
import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * */
@Component
public class PaymentResponseImpl implements Response {
    private String username;
    private String item;
    private Integer discount;
    private Double amount;

    /**
     * @see "Default Constructor"
     */
    public PaymentResponseImpl() {
    }

    /**
     * @see "Parameterized constructor"
     * @param username of user
     * @param item of product
     * @param discount on purchase
     * @param amount paid
     */
    public PaymentResponseImpl(String username, String item, Integer discount, Double amount) {
        this.username = username;
        this.item = item;
        this.discount = discount;
        this.amount = amount;
    }

    /**
     * @return username of user
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username of user
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return item of product
     */
    public String getItem() {
        return item;
    }

    /**
     * @param item of product
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * @return discount on purchase
     */
    public Integer getDiscount() {
        return discount;
    }

    /**
     * @param discount on purchase
     */
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    /**
     * @return amount paid
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount paid
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return string representation of entity
     */
    @Override
    public String toString() {
        return "PaymentResponse{" +
                "username='" + username + '\'' +
                ", item='" + item + '\'' +
                ", discount=" + discount +
                ", amount=" + amount +
                '}';
    }
}
