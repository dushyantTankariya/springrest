package com.snippet.bo.request;

import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Request Business Object"
 * */
@Component
public class PaymentRequest {

    private Integer userId;
    private Integer itemId;
    private Integer discount;
    private Double amount;

    /**
     * @see "Default Constructor"
     */
    public PaymentRequest() {
    }

    /**
     * @see "parametrized constructor"
     * @param userId of user
     * @param itemId of product
     * @param discount on purchase
     * @param amount paid
     */
    public PaymentRequest(Integer userId, Integer itemId, Integer discount, Double amount) {
        this.userId = userId;
        this.itemId = itemId;
        this.discount = discount;
        this.amount = amount;
    }

    /**
     * @return userId of user
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId of user
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return itemId of item
     */
    public Integer getItemId() {
        return itemId;
    }

    /**
     * @param itemId of item
     */
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    /**
     * @return discount on purchase
     */
    public Integer getDiscount() {
        return discount;
    }

    /**
     * @param discount on purchase
     */
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    /**
     * @return amount paid
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount paid
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return object of entity for database transaction
     */
    public Object[] getPaymentRequestToObjectArrayFactory(){
        return new Object[]{userId, itemId, discount, amount};
    }

    /**
     * @return String representation of entity
     */
    @Override
    public String toString() {
        return "PaymentRequest{" +
                "userId=" + userId +
                ", itemId=" + itemId +
                ", discount=" + discount +
                ", amount=" + amount +
                '}';
    }
}
