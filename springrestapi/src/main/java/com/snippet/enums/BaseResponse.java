package com.snippet.enums;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "constant Base Response"
 * */
public enum BaseResponse {
    /**
     * @see "Shared Secret key"
     */
    SHARED_KEY("SHARED_KEY"),
    /**
     * @see "http success status message"
     */
    SUCCESS_STATUS("success"),
    /**
     * @see "http error status message"
     * */
    ERROR_STATUS("error"),
    /**
     * @see "http sucess code"
     * */
    CODE_SUCCESS(200),
    /**
     * @see "http auth failure code"
     * */
    AUTH_FAILURE(102);

    public String value;

    /**
     * @param value of string enums
     */
    BaseResponse(String value){
        this.value = value;
    }

    public int intValue;

    /**
     * @param intValue of integer enums
     */
    BaseResponse(int intValue){
        this.intValue = intValue;
    }
}
